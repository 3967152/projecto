FROM openjdk:15-jdk

LABEL maintainer="Equipa 3"

COPY . /usr/src/myapp

WORKDIR /usr/src/myapp

RUN javac JavaApplication1/src/javaapplication1/JavaApplication1.java       

CMD ["java","JavaApplication1/src/javaapplication1/JavaApplication1.java"]
