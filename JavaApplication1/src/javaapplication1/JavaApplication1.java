
package javaapplication1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Formando
 */

public class JavaApplication1 {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		int tamMax = 100;
		int nItems = 0;
		int[] id = new int[tamMax];
		String[] modelo = new String[tamMax];
		double[] quantidade = new double[tamMax];
		double[] tamanhosapato = new double[tamMax];
		String[] genero = new String[tamMax];
		double[] preco = new double[tamMax];
		String[] modelotemp = new String[tamMax];
		double[] quantidadetemp = new double[tamMax];
		double[] tamanhosapatotemp = new double[tamMax];
		double[] precotemp = new double[tamMax];
		String[] generotemp = new String[tamMax];

		List<String> lista_ord = new ArrayList<String>();

		Scanner teclado = new Scanner(System.in);
		int op;
		do {
			System.out.println("\n1-Inserir sapatos:");
			System.out.println("2-Eliminar sapatos:");
			System.out.println("3-Listar todos os sapatos:");
			System.out.println("4-Editar sapatos:");
			System.out.println("5-Ordenar artigos por ordem crescente:");
			System.out.println("6-Ordenar artigos por ordem decrescente:");
			System.out.println("7-Sair.");
			op = teclado.next().charAt(0);
			teclado.nextLine();
			switch (op) {

			case '1':
				System.out.println("Insira o modelo do sapato");
				modelo[nItems] = teclado.nextLine();
				System.out.println("Insira a quantidade");
				quantidade[nItems] = teclado.nextDouble();
				System.out.println("Insira o preço");
				preco[nItems] = teclado.nextDouble();
				System.out.println("Insira o tamanho do sapato");
				tamanhosapato[nItems] = teclado.nextDouble();
				System.out.println("Insira o genero do sapato M/m-Masculino - F/f-Femenino");
				clearBuffer(teclado);
				genero[nItems] = teclado.nextLine();
				id[nItems] = nItems;
				lista_ord.add(modelo[nItems]);
				nItems++;
				break;

			case '2':
				System.out.println("Insira o id do sapato que quer eliminar");
				int idel = teclado.nextInt();
				for (int i = idel; i < nItems; i++) {
					modelo[i] = modelo[i + 1];
					quantidade[i] = quantidade[i + 1];
					tamanhosapato[i] = tamanhosapato[i + 1];
					preco[i] = preco[i + 1];
					genero[i] = genero[i + 1];
					id[i] = id[i + 1];
					nItems--;
				}
				break;

			case '3':
				System.out.println("ID\tModelo\t\tQuantidade\tPreço\t\tTamanho\t\tGenero");
				for (int i = 0; i < nItems; i++) {
					System.out.printf("%d: \t%-18s %.2f \t %.2f \t\t%.1f \t\t%s \n", id[i], modelo[i], quantidade[i],
							preco[i], tamanhosapato[i], genero[i]);
				}
				break;

			case '4':
				System.out.println("Insira o id do sapato que quer editar");
				int idtemp = teclado.nextInt();
				teclado.nextLine();
				System.out.println("Insira o novo modelo do sapato");
				modelotemp[nItems] = teclado.nextLine();
				System.out.println("Insira a nova quantidade de sapatos");
				quantidadetemp[nItems] = teclado.nextDouble();
				System.out.println("Insira o novo preço do sapato");
				precotemp[nItems] = teclado.nextDouble();
				System.out.println("Insira o novo tamanho do sapato");
				tamanhosapatotemp[nItems] = teclado.nextDouble();
				System.out.println("Insira o novo genero do sapato M/m-Masculino - F/f-Femenino");
				clearBuffer(teclado);
				generotemp[nItems] = teclado.nextLine();

				for (int i = 0; i < nItems; i++) {
					if (idtemp == id[i]) {
						modelo[i] = modelotemp[nItems];
						quantidade[i] = quantidadetemp[nItems];
						preco[i] = precotemp[nItems];
						tamanhosapato[i] = tamanhosapatotemp[nItems];
						genero[i] = generotemp[nItems];
						lista_ord.add(i, modelotemp[nItems]);
					} else
						System.out.println("erro");
				}
				break;

			case '5':
				Collections.sort(lista_ord);
				for (String counter : lista_ord) {
					System.out.println(counter);
				}
				break;

			case '6':
				Collections.sort(lista_ord, Collections.reverseOrder());
				for (String counter : lista_ord) {
					System.out.println(counter);
				}
				break;
			}
		} while (op != '7');
	}

	private static void clearBuffer(Scanner teclado) {
		if (teclado.hasNextLine()) {
			teclado.nextLine();
		}
	}

}
